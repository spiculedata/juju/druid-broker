import os
import stat
import math
from subprocess import run as sub_run
from psutil import virtual_memory
from charms.reactive import when, when_not, when_any, is_flag_set, set_flag, clear_flag, endpoint_from_flag
from charms.templating.jinja2 import render
from charmhelpers.core.hookenv import resource_get, status_set, log
from subprocess import check_call


@when_not('druid-broker.installed')
def install_druid_broker():
    memory_check = check_sufficient_memory()
    if not memory_check[0]:
        log('Can not start MiddleManager, server has '
            + memory_check[1] + 'GB of RAM, but Broker requires at least ' + memory_check[2] + 'GB')
        status_set('blocked', 'Broker has insufficient RAM '
                              '(has ' + memory_check[1] + 'GB, requires ' + memory_check[2] + 'GB)')
    else:
        archive = resource_get("druid")
        if not os.path.isdir('/opt/druid_broker'):
            os.mkdir('/opt/druid_broker')
        cmd = ['tar', 'xfz', archive, '-C', '/opt/druid_broker', '--strip', '1']
        check_call(cmd)

        archive = resource_get("mysql-extension")
        cmd = ['tar', 'xfz', archive, '-C', '/opt/druid_broker/extensions']
        check_call(cmd)

        os.mkdir('/opt/druid_broker/var')
        os.mkdir('/opt/druid_broker/var/tmp')
        os.mkdir('/opt/druid_broker/var/druid')
        os.mkdir('/opt/druid_broker/var/druid/indexing-logs')
        os.mkdir('/opt/druid_broker/var/druid/segments')
        os.mkdir('/opt/druid_broker/var/druid/segment-cache')
        os.mkdir('/opt/druid_broker/var/druid/task')
        os.mkdir('/opt/druid_broker/var/druid/hadoop-tmp')
        os.mkdir('/opt/druid_broker/var/druid/pids')
        os.mkdir('/opt/druid_broker/log')

        render('druid_broker', '/etc/init.d/druid_broker')
        render('druid_logrotate', '/etc/logrotate.d/druid_logrotate')
        st = os.stat('/etc/init.d/druid_broker')
        os.chmod('/etc/init.d/druid_broker', st.st_mode | stat.S_IEXEC)

        set_flag('druid-broker.installed')
        status_set('waiting', 'Waiting for config file')


@when('druid-broker.installed', 'endpoint.config.new_config')
def configure_druid_broker():
    with open('/opt/druid_broker/conf/druid/_common/common.runtime.properties', 'r') as c:
        old_conf = c.read()

    config = endpoint_from_flag('endpoint.config.new_config')
    new_conf = config.get_config()

    if old_conf != new_conf:
        log('New config detected! Resetting Druid Broker.')
        if is_flag_set('druid-broker.configured'):
            clear_flag('druid-broker.configured')

        status_set('maintenance', 'Configuring Broker')

        config_file = open('/opt/druid_broker/conf/druid/_common/common.runtime.properties', 'w')
        config_file.write(new_conf)
        config_file.close()

        # write_runtime_properties()
        write_jvm_config()

        set_flag('druid-broker.configured')
        set_flag('druid-broker.new_config')
        status_set('maintenance', 'Druid Broker configured, waiting to start process...')


@when('druid-broker.installed', 'endpoint.config.new_hdfs_files')
@when_not('druid-broker.hdfs_configured')
def configure_hdfs_files():
    hdfs = endpoint_from_flag('endpoint.config.new_hdfs_files')
    new_hdfs_files = hdfs.get_hadoop_files()

    status_set('maintenance', 'Copying HDFS XML Files.')

    with open('/opt/druid_broker/conf/druid/_common/core-site.xml', 'w') as f:
        f.write(new_hdfs_files[0])
    with open('/opt/druid_broker/conf/druid/_common/hdfs-site.xml', 'w') as f:
        f.write(new_hdfs_files[1])
    with open('/opt/druid_broker/conf/druid/_common/mapred-site.xml', 'w') as f:
        f.write(new_hdfs_files[2])
    with open('/opt/druid_broker/conf/druid/_common/yarn-site.xml', 'w') as f:
        f.write(new_hdfs_files[3])

    if is_flag_set('druid-broker.configured'):
        clear_flag('druid-broker.configured')

    set_flag('druid-broker.hdfs_configured')
    set_flag('druid-broker.new_config')
    status_set('waiting', 'HDFS files copied. Waiting...')


@when_any('druid-broker.configured', 'druid-broker.hdfs_configured')
@when('java.ready', 'druid-broker.new_config')
def run_druid_broker(java):
    restart_broker()
    clear_flag('druid-broker.new_config')


def start_broker():
    status_set('maintenance', 'Starting Broker...')
    code = sub_run(['/etc/init.d/druid_broker', 'start'], cwd='/opt/druid_broker')
    if code.returncode == 0:
        set_flag('druid-broker.running')
        status_set('active', 'Broker running')
    else:
        status_set('blocked', 'Error starting Broker')


def stop_broker():
    status_set('maintenance', 'Stopping Broker')
    code = sub_run(['/etc/init.d/druid_broker', 'stop'], cwd='/opt/druid_broker')
    if code.returncode == 0:
        clear_flag('druid-broker.running')
        status_set('waiting', 'Broker stopped')
    else:
        status_set('blocked', 'Error stopping Broker')


def restart_broker():
    status_set('maintenance', 'Restarting Broker...')
    stop_broker()
    start_broker()


def check_minimum_memory():
    """
    Calculates the minimum memory requirement for this Druid Broker instance. Rule of thumb is:

            Minimum Requirement (bytes) = sizeBytes * (numThreads + numMergeBuffers + 1)

    However, we provide an extra GB of headroom on the low end to ensure that Druid Broker will be able to run.
    This additional GB is added within the return statement.

    :return: The minimum memory requirements (essentially Xms) of this Druid instance in bytes.
    """

    if os.cpu_count() > 1:
        numThreads = os.cpu_count() - 1
    else:
        numThreads = 1

    # numMergeBuffers is max of 2, numThreads/4
    if numThreads // 4 > 2:
        numMergeBuffers = numThreads // 4
    else:
        numMergeBuffers = 2

    sizeBytes = 1073741824  # 1 GB, parametize this later

    return sizeBytes * (numMergeBuffers + numThreads + 1) + 1073741824


def write_runtime_properties():

    context = {
        'mdms': check_minimum_memory()
    }

    render('runtime.properties', '/opt/druid_broker/conf/druid/broker/runtime.properties', context)


def write_jvm_config():

    mem_gb = check_system_memory() // (1024 ** 3)  # Total memory of server, floor division for additional overhead
    Xms = math.ceil(check_minimum_memory() / (1024 ** 3))  # Minimum memory requirement for Druid in GB
    Xmx = max(Xms, math.floor(mem_gb - 1))  # Allocate maximum memory from total, subtract 1GB for overhead or set Xms

    context = {
        'Xms': Xms,
        'Xmx': Xmx
    }

    render('jvm.config', '/opt/druid_broker/conf/druid/broker/jvm.config', context)


def check_system_memory():
    """
    Returns the total system memory in bytes.
    """

    mem = virtual_memory()
    return mem.total


def check_sufficient_memory():
    """
    This method checks to make sure there is sufficient memory before allowing the charm to continue installing.
    """

    # Get minimum memory requirement for the heap. Round up since we'll round up when allocating Xms
    heap = math.ceil(check_minimum_memory() // (1024 ** 3))

    # Get total memory of the system
    mem = check_system_memory() // (1024 ** 3)

    # If the memory requirements exceed the amount available
    if heap > mem:
        return False, str(mem), str(int(heap))
    else:
        return True, 0, 0
