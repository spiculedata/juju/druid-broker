# Overview

Welcome to the Druid Broker charm by Spicule. The Broker is the node to route queries to if you want to run a distributed cluster. It understands 
the metadata published to ZooKeeper about what segments exist on what nodes and routes queries such that they hit the right nodes. This node also 
merges the result sets from all of the individual nodes together. On start up, Realtime nodes announce themselves and the segments they are serving in Zookeeper. 
For more information on Druid Broker, please visit the [Druid Broker Documentation](http://druid.io/docs/latest/design/broker.html).

# Usage

To deploy this charm from the command line, enter the following:

    juju deploy cs:~spiculecharms/druid-broker --series xenial

You can specify hardware constraints for your Broker node on the command
line. To find out more, refer to the ["Using Constraints"](https://docs.jujucharms.com/2.4/en/charms-constraints").
For example, to deploy your Broker node on a server with 4 cores and 15 GB
of RAM, enter the following command:

    juju deploy cs:~spiculecharms/druid-broker --series xenial --constraints "mem=15G cores=4"

# OpenJDK and Druid Config

Druid Broker requires a relation to the [OpenJDK charm](https://jujucharms.com/openjdk/)
to automate the installation of Java onto Brokers server, which is a
requirement of the Broker.

Additionally, Broker requires configuration as part of the wider Druid
cluster, so Broker must be related to our [Druid Config charm](https://jujucharms.com/new/u/spiculecharms/druid-config).
The individual Druid nodes do not need to be directly related to one another,
and should instead be directly related to Druid Config instead.

# Configuration

Druid Cluster configuration is handled through the [Druid Config charm](https://jujucharms.com/new/u/spiculecharms/druid-config).

# Contact Information

Thank you for using the Druid Coordinator charm - we hope you find it useful! For
more information, here are our contact details:

Spicule: [http://www.spicule.co.uk](http://www.spicule.co.uk)
Anssr: [http://anssr.io](http://anssr.io)
Email: info@spicule.co.uk

For assistance with Juju, please refer to the [Juju Discourse](https://discourse.jujucharms.com/).

## Druid

For more information about Druid, please refer to the [Druid website](http://druid.io).
